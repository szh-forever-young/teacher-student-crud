import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/student/admin/system/index/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/student/admin/system/index/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/student/admin/system/index/logout',
    method: 'post'
  })
}
