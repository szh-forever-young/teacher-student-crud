import request from '@/utils/request'

const api_name = '/student'

export default {
    //根据id查询学生
    findStudentById(id) {
        return request({
            url: `${api_name}/findStudentById/${id}`,
            method: 'get'
        })
    },
    //查询所有学生
    findAllStudents() {
        return request({
            url: `${api_name}/findAllStudents`,
            method: 'get'
        })
    },
    //条件查询学生，带分页
    findStudentsByConditionPage(page, limit, searchObj) {
        return request({
            url: `${api_name}/findStudentsByConditionPage/${page}/${limit}`,
            method: 'post',
            data: searchObj
        })
    },
    //新增学生
    saveStudent(student) {
        return request({
            url: `${api_name}/saveStudent`,
            method: 'post',
            data: student
        })
    },
    //根据id修改学生
    updateStudentById(student) {
        return request({
            url: `${api_name}/updateStudent`,
            method: 'put',
            data: student
        })
    },
    //根据id逻辑删除学生
    deleteStudentById(id) {
        return request({
            url: `${api_name}/deleteStudentById/${id}`,
            method: 'patch'
        })
    },
    //批量删除学生
    deleteBatchByIds(ids) {
        return request({
            url: `${api_name}/deleteBatchByIds`,
            method: 'patch',
            data: ids
        })
    }
}
