import request from '@/utils/request'

const api_name = '/teacher'

export default {
    //根据id查询教师
    selectTeacherById(id) {
        return request({
            url: `${api_name}/selectTeacherById/${id}`,
            method: 'get'
        })
    },
    //查询所有教师
    selectAll() {
        return request({
            url: `${api_name}/selectAll`,
            method: 'get'
        })
    },
    //条件查询教师，带分页
    selectTeachersByConditionPage(page, limit, searchObj) {
        return request({
            url: `${api_name}/selectTeachersByConditionPage/${page}/${limit}`,
            method: 'post',
            data: searchObj
        })
    },
    //新增教师
    insertTeacher(teacher) {
        return request({
            url: `${api_name}/insertTeacher`,
            method: 'post',
            data: teacher
        })
    },
    //根据id修改教师
    modifyTeacher(teacher) {
        return request({
            url: `${api_name}/modifyTeacher`,
            method: 'put',
            data: teacher
        })
    },
    //根据id逻辑删除教师
    removeTeacherById(id) {
        return request({
            url: `${api_name}/removeTeacherById/${id}`,
            method: 'patch'
        })
    },
    //批量删除教师
    removeBatchByIds(ids) {
        return request({
            url: `${api_name}/removeBatchByIds`,
            method: 'patch',
            data: ids
        })
    }
}