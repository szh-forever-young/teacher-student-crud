## 项目大体介绍



1. **项目名：SpringBoot + SpringCloud + Vue 实现教师学生的 CRUD**

2. **项目概述：**基于 SpringBoot、SpringCloud、MySQL、Maven、MyBatis、Swagger、Vue、Element-ui 实现针对教师学生的 CRUD，接口包括：单个查询、查所有、条件分页查询、新增、修改、单个逻辑删除、批量逻辑删除、远程接口调用。

3. **项目整体架构：**

   - **前端：Vue + Element-ui；**
   - **后端：**
     - **service-edu-parent（总父工程）**
       - **api-gateway（网关模块，拦截页面发来的请求、转发至对应的微服务模块、解决跨域）**
       - **common-util（公共模块，存放公共配置类、公共枚举类、统一异常处理器、自定义全局异常类、统一结果返回类）**
       - **model（实体数据模块，存放数据库表对应的实体类、封装接口入参的相关 vo 类）**
       - **service-edu（核心业务模块）**
         - **service-edu-student（学生模块）**
         - **service-edu-teacher（教师模块）**

4. **项目所使用到的技术：**

   - **前端：**
     - 学生教师后台管理系统采用 **Vue** 中的 **vue-admin-template** 模板搭建；
     - 页面中的各个组件、按钮、菜单栏等使用 **Element-ui** 整合。
   - **后端：**
     - 整体采用 Spring Framework 搭建，即 **SpringBoot + SpringCloud**，其中使用 **SpringCloud** 中的 **OpenFeign 组件做远程接口调用**、**Gateway 组件做网关进行拦截转发**，使用 **SpringCloud Alibaba** 中的 **Nacos 组件做服务注册中心；**
     - 持久层采用 **MyBatis**，同时整合 **PageHelper 分页插件；**
     - 数据库采用 **MySQL；**
     - 项目采用 **Maven 多模块管理**方式搭建（总父工程为 service-edu-parent，api-gateway 为网关模块，common-util 为公共模块，model 为实体类 vo 数据模块，service-edu 为核心业务模块，service-edu-student 为学生模块，service-edu-teacher 为教师模块）；
     - 项目采用 **Git 进行分布式代码管理；**
     - API接口文档采用 **Swagger实现；**

5. **项目实现的功能：**

   - api-gateway 网关模块、service-edu-student 学生模块、service-edu-teacher 教师模块都会注册到 Nacos 注册中心，以便其他微服务模块可以发现进行请求转发、远程调用。

   - 前端页面发来请求，被 api-gateway 网关模块拦截，根据请求具体的 url 进行断言机制的判断，将请求转发至对应的核心业务模块（student、teacher）。

   - 针对学生的单个查询、查所有、条件分页查询、新增、修改、单个逻辑删除、批量逻辑删除、远程接口调用。

   - 针对教师的单个查询、查所有、条件分页查询、新增、修改、单个逻辑删除、批量逻辑删除、远程接口调用。

   - 所有业务方法的日志信息全部采用如下方式进行打印：

     ```java
     private static final Logger LOGGER = LoggerFactory.getLogger(XXX.class);
     ```

     