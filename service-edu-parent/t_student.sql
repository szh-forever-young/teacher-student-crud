/*
 Navicat Premium Data Transfer

 Source Server         : MyProject
 Source Server Type    : MySQL
 Source Server Version : 50605
 Source Host           : localhost:3306
 Source Schema         : education

 Target Server Type    : MySQL
 Target Server Version : 50605
 File Encoding         : 65001

 Date: 03/07/2023 14:11:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_student
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '学生id，主键自增',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '学生姓名',
  `age` int(11) NOT NULL COMMENT '学生年龄',
  `sex` tinyint(1) NOT NULL COMMENT '学生性别，1：男，2：女',
  `intro` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '学生简介',
  `hobby` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '学生爱好',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '学生住址',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除，0：未删除，1：已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_student
-- ----------------------------
INSERT INTO `t_student` VALUES (1, '张三', 26, 1, '好学生', '写代码', '广东省深圳市南山区', 0, '2023-05-05 15:23:57', '2023-05-08 15:36:02');
INSERT INTO `t_student` VALUES (2, '李四', 18, 1, '坏学生', '打游戏', '浙江省杭州市余杭区', 0, '2023-05-05 15:24:37', '2023-05-07 14:53:06');
INSERT INTO `t_student` VALUES (3, '王五', 25, 2, '不好不坏的学生', '压马路', '湖北省武汉市江汉区', 0, '2023-05-05 15:25:28', '2023-05-07 14:53:07');
INSERT INTO `t_student` VALUES (4, '赵六', 21, 1, '三好学生', '玩手机', '陕西省西安市雁塔区', 0, '2023-05-05 15:26:32', '2023-05-07 14:53:08');
INSERT INTO `t_student` VALUES (5, '田七', 17, 2, '黑丝御姐', '逛街，吃好吃的', '河南省洛阳市涧西区', 0, '2023-05-07 10:50:54', '2023-05-07 15:16:20');
INSERT INTO `t_student` VALUES (8, '张老四', 23, 1, '小伙子还不错哦', '上网，烫头', '河南省洛阳市老城区', 0, '2023-05-07 14:55:22', '2023-05-08 14:06:38');
INSERT INTO `t_student` VALUES (9, '小红', 15, 2, '初三学生', '化妆，逛街，吃喝玩', '河南省郑州市金水区', 0, '2023-05-08 14:07:28', '2023-05-08 14:07:28');
INSERT INTO `t_student` VALUES (10, '小明', 16, 1, '高一学生', '上网，玩手机', '河南省洛阳市西工区', 0, '2023-05-08 14:09:38', '2023-05-08 14:09:38');
INSERT INTO `t_student` VALUES (11, '小军', 10, 1, '四年级小学生', '王者荣耀，三国杀', '河南省洛阳市洛龙区', 0, '2023-05-08 14:10:25', '2023-05-08 14:10:24');
INSERT INTO `t_student` VALUES (12, '小强', 18, 1, '大一新生', '撩妹', '河南省郑州市二七区', 0, '2023-05-08 14:10:56', '2023-05-08 14:10:55');
INSERT INTO `t_student` VALUES (13, '小亮', 18, 1, '高三学生', '写作业，考试', '河南省洛阳市瀍河区', 0, '2023-05-08 14:12:08', '2023-05-08 14:12:08');
INSERT INTO `t_student` VALUES (14, '小丽', 22, 2, '大四毕业生', '逛淘宝，逛京东，逛拼多多', '河南省郑州市中原区', 0, '2023-05-08 14:14:26', '2023-05-08 14:14:25');
INSERT INTO `t_student` VALUES (15, '888', 1, 2, 'string', 'string', 'string', 0, '2023-06-30 11:09:10', '2023-06-30 11:22:47');
INSERT INTO `t_student` VALUES (16, 'string', 1, 2, 'string', 'string', 'string', 0, '2023-06-30 11:21:19', '2023-06-30 11:22:51');

SET FOREIGN_KEY_CHECKS = 1;
