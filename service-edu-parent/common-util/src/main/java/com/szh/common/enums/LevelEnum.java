package com.szh.common.enums;

import lombok.Getter;

/**
 * 教师职称枚举类
 *
 * @author: SongZiHao
 * @date: 2023/5/8
 */
@Getter
public enum LevelEnum {
    助教(1, "ASSISTANT"),
    讲师(2, "LECTURER"),
    副教授(3, "ASSOCIATE PROFESSOR"),
    教授(4, "PROFESSOR");

    private Integer levelId;
    private String levelInfo;

    LevelEnum(Integer levelId, String levelInfo) {
        this.levelId = levelId;
        this.levelInfo = levelInfo;
    }
}
