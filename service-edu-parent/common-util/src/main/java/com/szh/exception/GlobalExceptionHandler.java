package com.szh.exception;

import com.szh.result.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理类
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = ServiceEduException.class)
    public Result error(ServiceEduException ex) {
        LOGGER.error("ServiceEduException ===> {}", ex.getErrorMsg());
        return Result.fail().code(ex.getErrorCode()).message(ex.getErrorMsg());
    }

    @ExceptionHandler(value = NullPointerException.class)
    public Result error(NullPointerException ex) {
        LOGGER.error("NullPointerException ===> {}", ex.getMessage());
        return Result.fail(ex.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public Result error(Exception ex) {
        LOGGER.error("Exception ===> {}", ex.getMessage());
        return Result.fail(ex.getMessage());
    }
}
