package com.szh.exception;

import com.szh.enums.ResultCodeEnum;
import lombok.Data;

/**
 * 自定义异常类
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@Data
public class ServiceEduException extends RuntimeException {

    /**
     * 异常状态码
     */
    private Integer errorCode;

    /**
     * 异常错误信息
     */
    private String errorMsg;

    public ServiceEduException() {
        super();
    }

    public ServiceEduException(Integer errorCode, String errorMsg) {
        super(errorMsg);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public ServiceEduException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.errorCode = resultCodeEnum.getCode();
        this.errorMsg = resultCodeEnum.getMessage();
    }
}
