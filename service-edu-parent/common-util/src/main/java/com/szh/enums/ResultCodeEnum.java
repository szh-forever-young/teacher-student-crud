package com.szh.enums;

import lombok.Getter;

/**
 * 响应状态枚举类
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@Getter
public enum ResultCodeEnum {
    SUCCESS(20000, "Success"),
    QUERY_ONE_NOT_FOUND(20001, "Query One Not Found"),
    QUERY_LIST_NOT_FOUND(20002, "Query List Not Found"),
    ILLEGAL_PARAM(20003, "Illegal Param"),
    RECORD_ALREADY_EXISTS(20004, "Record Already Exists"),
    INSERT_FAILED(20005, "Insert Failed"),
    UPDATE_FAILED(20006, "Update Failed"),
    DELETE_FAILED(20007, "Delete Failed"),
    SAME_PROPERTIES_EXIST(20008, "Same Properties exist"),
    OTHER_REASON(20009, "Other Reason"),
    SERVICE_ERROR(44444, "Service Error");

    private Integer code;
    private String message;

    ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
