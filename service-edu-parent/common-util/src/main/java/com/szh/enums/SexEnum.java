package com.szh.enums;

import lombok.Getter;

/**
 * @author: SongZiHao
 * @date: 2023/5/7
 */
@Getter
public enum SexEnum {
    MAN(1, "男"),
    WOMAN(2, "女");

    private Integer sexId;
    private String sexInfo;

    SexEnum(Integer sexId, String sexInfo) {
        this.sexId = sexId;
        this.sexInfo = sexInfo;
    }
}
