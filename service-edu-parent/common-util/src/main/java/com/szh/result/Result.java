package com.szh.result;

import com.szh.enums.ResultCodeEnum;
import lombok.Data;

/**
 * 统一结果返回类
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@Data
public class Result<T> {
    /**
     * 响应状态码
     */
    private Integer code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应具体数据
     */
    private T data;

    /**
     * 构造方法私有化，防止外部new对象
     */
    private Result() {
    }

    /**
     * 提供一个受保护的方法，设置响应具体数据data
     *
     * @param data 响应具体数据
     * @param <T> <T>泛型
     * @return result
     */
    protected static <T> Result<T> build(T data) {
        Result<T> result = new Result<>();
        if (null != data) {
            result.setData(data);
        }
        return result;
    }

    /**
     * 传入具体的body、code、message，构建Result对象
     *
     * @param body 响应具体数据
     * @param code 响应状态码
     * @param message 响应消息
     * @param <T> <T>泛型
     * @return result
     */
    public static <T> Result<T> build(T body, Integer code, String message) {
        Result<T> result = build(body);
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    /**
     * 传入具体的枚举类，构建Result对象
     *
     * @param body 响应具体数据
     * @param resultCodeEnum 响应状态枚举类
     * @param <T> <T>泛型
     * @return result
     */
    public static <T> Result<T> build(T body, ResultCodeEnum resultCodeEnum) {
        Result<T> result = build(body);
        result.setCode(resultCodeEnum.getCode());
        result.setMessage(resultCodeEnum.getMessage());
        return result;
    }

    /**
     * 请求响应成功、没有data数据的方法
     *
     * @param <T> <T>泛型
     * @return result
     */
    public static <T> Result<T> ok() {
        return Result.ok(null);
    }

    /**
     * 请求响应成功、有data数据的方法
     *
     * @param data 响应具体数据
     * @param <T> <T>泛型
     * @return result
     */
    public static <T> Result<T> ok(T data) {
        Result<T> result = build(data);
        return build(data, ResultCodeEnum.SUCCESS);
    }

    /**
     * 请求响应失败、没有data数据的方法
     *
     * @param <T> <T>泛型
     * @return result
     */
    public static <T> Result<T> fail() {
        return Result.fail(null);
    }

    /**
     * 请求响应失败、有data数据的方法
     *
     * @param data 响应具体数据
     * @param <T> <T>泛型
     * @return result
     */
    public static <T> Result<T> fail(T data) {
        Result<T> result = build(data);
        return build(data, ResultCodeEnum.SERVICE_ERROR);
    }

    /**
     * 可以自定义code响应状态码，传入Result对象进行构建
     *
     * @param code 响应状态码
     * @return 当前result对象
     */
    public Result<T> code(Integer code) {
        this.setCode(code);
        return this;
    }

    /**
     * 可以自定义message响应消息，传入Result对象进行构建
     *
     * @param message 响应消息
     * @return 当前result对象
     */
    public Result<T> message(String message) {
        this.setMessage(message);
        return this;
    }
}
