package com.szh.edu.student.controller;

import com.szh.edu.student.service.StudentService;
import com.szh.common.enums.ResultCodeEnum;
import com.szh.model.Student;
import com.szh.common.result.Result;
import com.szh.model.Teacher;
import com.szh.vo.StudentQueryVo;
import com.szh.vo.StudentSaveVo;
import com.szh.vo.StudentUpdateVo;
import com.szh.vo.TeacherQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@Api(tags = "学生管理")
@RestController
@RequestMapping(value = "/student")
public class StudentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @ApiOperation(value = "根据id查询学生")
    @GetMapping(value = "/findStudentById/{id}")
    public Result<Student> findStudentById(@ApiParam(name = "id", value = "学生id", required = true)
                                           @PathVariable("id") Long id) {
        Student student = studentService.findStudentById(id);
        return Result.build(student, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "查询所有学生")
    @GetMapping(value = "/findAllStudents")
    public Result<List<Student>> findAllStudents() {
        List<Student> students = studentService.findAllStudents();
        return Result.build(students, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "条件查询学生，带分页")
    @PostMapping(value = "/findStudentsByConditionPage/{page}/{limit}")
    public Result<Map<String, Object>> findStudentsByConditionPage(@ApiParam(name = "page", value = "页码", required = true)
                                                                   @PathVariable("page") Long page,
                                                                   @ApiParam(name = "limit", value = "每页记录数", required = true)
                                                                   @PathVariable("limit") Long limit,
                                                                   @ApiParam(name = "studentQueryVo", value = "查询条件vo类")
                                                                   @RequestBody(required = false) StudentQueryVo studentQueryVo) {
        Map<String, Object> resultMap = studentService.findStudentsByConditionPage(page, limit, studentQueryVo);
        return Result.ok(resultMap);
    }

    @ApiOperation(value = "新增学生")
    @PostMapping(value = "/saveStudent")
    public Result saveStudent(@ApiParam(name = "studentSaveVo", value = "学生新增实体", required = true)
                              @RequestBody StudentSaveVo studentSaveVo) {
        return studentService.saveStudent(studentSaveVo) ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "根据id修改学生")
    @PutMapping(value = "/updateStudent")
    public Result updateStudentById(@ApiParam(name = "studentUpdateVo", value = "学生修改实体", required = true)
                                    @RequestBody StudentUpdateVo studentUpdateVo) {
        return studentService.updateStudent(studentUpdateVo) ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "根据id逻辑删除学生")
    @PatchMapping(value = "/deleteStudentById/{id}")
    public Result deleteStudentById(@ApiParam(name = "id", value = "待删学生id", required = true)
                                    @PathVariable("id") Long id) {
        return studentService.deleteStudentById(id) ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "批量删除学生")
    @PatchMapping(value = "/deleteBatchByIds")
    public Result deleteBatchByIds(@ApiParam(name = "studentIds", value = "待删学生id集合", required = true)
                                   @RequestBody List<Long> studentIds) {
        return studentService.deleteBatchByIds(studentIds) ? Result.ok() : Result.fail();
    }

    //--------------------------- 以下接口只为回顾OpenFeign远程接口调用 ---------------------------

    @ApiOperation(value = "远程接口调用：根据id查询教师")
    @GetMapping(value = "/openFeign/teacher/selectTeacherById/{id}")
    public Result<Teacher> openFeignSelectTeacherById(@ApiParam(name = "id", value = "教师id", required = true)
                                                      @PathVariable("id") Long id) {
        Teacher teacher = studentService.openFeignSelectTeacherById(id);
        return Result.build(teacher, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "远程接口调用：查询所有教师")
    @GetMapping(value = "/openFeign/teacher/selectAll")
    public Result<List<Teacher>> openFeignSelectAll() {
        List<Teacher> teacherList = studentService.openFeignSelectAll();
        return Result.build(teacherList, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "远程接口调用：条件查询教师，带分页")
    @PostMapping(value = "/openFeign/teacher/selectTeachersByConditionPage/{page}/{limit}")
    public Result<Map<String, Object>> openFeignSelectTeachersByConditionPage(@ApiParam(name = "page", value = "页码", required = true)
                                                                              @PathVariable("page") Long page,
                                                                              @ApiParam(name = "limit", value = "每页记录数", required = true)
                                                                              @PathVariable("limit") Long limit,
                                                                              @ApiParam(name = "teacherQueryVo", value = "查询条件vo类")
                                                                              @RequestBody(required = false) TeacherQueryVo teacherQueryVo) {
        Map<String, Object> map = studentService.openFeignSelectTeachersByConditionPage(page, limit, teacherQueryVo);
        return Result.ok(map);
    }
}
