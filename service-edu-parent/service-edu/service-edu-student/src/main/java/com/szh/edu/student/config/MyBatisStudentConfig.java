package com.szh.edu.student.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@Configuration
@MapperScan(basePackages = "com.szh.edu.student.mapper")
@ComponentScan(basePackages = "com.szh")
public class MyBatisStudentConfig {

}
