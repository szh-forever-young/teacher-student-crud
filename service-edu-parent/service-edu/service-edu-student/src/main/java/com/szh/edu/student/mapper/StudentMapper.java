package com.szh.edu.student.mapper;

import com.szh.model.Student;
import com.szh.vo.StudentQueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2023/5/6
 */
public interface StudentMapper {
    /**
     * 根据id查询学生
     * @param id 学生id
     * @return Student
     */
    Student findStudentById(@Param("id") Long id);

    /**
     * 查询所有学生
     * @return List<Student> 学生列表
     */
    List<Student> findAllStudents();

    /**
     * 条件查询学生，带分页
     * @param studentQueryVo 查询条件
     * @return List<Student> 学生列表
     */
    List<Student> findStudentsByConditionPage(StudentQueryVo studentQueryVo);

    /**
     * 新增学生
     * @param student 学生实体
     * @return 新增之后的受影响行数，0失败，1成功
     */
    int insertStudent(Student student);

    /**
     * 根据id修改学生
     * @param student 学生实体
     * @return 修改之后的受影响行数，0失败，1成功
     */
    int updateStudent(Student student);

    /**
     * 根据id逻辑删除学生
     * @param id 待删学生id
     * @return 删除之后的受影响行数，0失败，1成功
     */
    int deleteStudentById(@Param("id") Long id);

    /**
     * 批量删除学生
     * @param studentIds 待删学生id集合
     * @return 批量删除之后的受影响行数，等于studentIds集合长度则表示成功，其余情况均表示失败
     */
    int deleteBatchByIds(@Param("studentIds") List<Long> studentIds);
}
