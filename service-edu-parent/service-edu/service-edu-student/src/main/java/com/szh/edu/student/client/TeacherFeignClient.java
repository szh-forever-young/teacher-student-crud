package com.szh.edu.student.client;

import com.szh.common.result.Result;
import com.szh.model.Teacher;
import com.szh.vo.TeacherQueryVo;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 远程调用 service-edu-teacher 模块的接口
 *
 * @author: SongZiHao
 * @date: 2023/7/3
 */
@FeignClient("service-edu-teacher")
public interface TeacherFeignClient {

    @GetMapping(value = "/teacher/selectTeacherById/{id}")
    Result<Teacher> selectTeacherById(@PathVariable("id") Long id);

    @GetMapping(value = "/teacher/selectAll")
    Result<List<Teacher>> selectAll();

    @PostMapping(value = "/teacher/selectTeachersByConditionPage/{page}/{limit}")
    Result<Map<String, Object>> selectTeachersByConditionPage(@PathVariable("page") Long page,
                                                              @PathVariable("limit") Long limit,
                                                              @RequestBody(required = false) TeacherQueryVo teacherQueryVo);
}
