package com.szh.edu.student.service;

import com.github.pagehelper.PageInfo;
import com.szh.model.Student;
import com.szh.model.Teacher;
import com.szh.vo.StudentQueryVo;
import com.szh.vo.StudentSaveVo;
import com.szh.vo.StudentUpdateVo;
import com.szh.vo.TeacherQueryVo;

import java.util.List;
import java.util.Map;

/**
 * @author: SongZiHao
 * @date: 2023/5/6
 */
public interface StudentService {

    /**
     * 根据id查询学生
     * @param id 学生id
     * @return Student
     */
    Student findStudentById(Long id);

    /**
     * 查询所有学生
     * @return List<Student> 学生列表
     */
    List<Student> findAllStudents();

    /**
     * 条件查询学生，带分页
     * @param page 页码
     * @param limit 每页记录数
     * @param studentQueryVo 查询条件
     * @return Map<String, Object>
     */
    Map<String, Object> findStudentsByConditionPage(Long page, Long limit, StudentQueryVo studentQueryVo);

    /**
     * 新增学生
     * @param studentSaveVo 学生新增vo类
     * @return 是否新增成功
     */
    boolean saveStudent(StudentSaveVo studentSaveVo);

    /**
     * 根据id修改学生
     * @param studentUpdateVo 学生修改vo类
     * @return 是否修改成功
     */
    boolean updateStudent(StudentUpdateVo studentUpdateVo);

    /**
     * 根据id逻辑删除学生
     * @param id 待删学生id
     * @return 是否删除成功
     */
    boolean deleteStudentById(Long id);

    /**
     * 批量删除学生
     * @param studentIds 待删学生id集合
     * @return 是否批量删除成功
     */
    boolean deleteBatchByIds(List<Long> studentIds);

    /**
     * 远程接口调用：根据id查询教师
     * @param id 教师id
     * @return Teacher
     */
    Teacher openFeignSelectTeacherById(Long id);

    /**
     * 远程接口调用：查询所有教师
     * @return List<Teacher> 教师列表
     */
    List<Teacher> openFeignSelectAll();

    /**
     * 远程接口调用：条件查询教师，带分页
     * @param page 页码
     * @param limit 每页记录数
     * @param teacherQueryVo 查询条件
     * @return Map<String, Object>
     */
    Map<String, Object> openFeignSelectTeachersByConditionPage(Long page, Long limit, TeacherQueryVo teacherQueryVo);
}
