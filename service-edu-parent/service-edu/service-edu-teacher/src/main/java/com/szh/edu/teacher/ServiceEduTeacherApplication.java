package com.szh.edu.teacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@EnableTransactionManagement
public class ServiceEduTeacherApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceEduTeacherApplication.class, args);
    }
}
