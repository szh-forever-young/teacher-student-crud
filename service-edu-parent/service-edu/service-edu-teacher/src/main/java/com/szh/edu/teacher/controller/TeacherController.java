package com.szh.edu.teacher.controller;

import com.szh.edu.teacher.service.TeacherService;
import com.szh.common.enums.ResultCodeEnum;
import com.szh.model.Student;
import com.szh.model.Teacher;
import com.szh.common.result.Result;
import com.szh.vo.StudentQueryVo;
import com.szh.vo.TeacherInsertVo;
import com.szh.vo.TeacherQueryVo;
import com.szh.vo.TeacherUpdateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author: SongZiHao
 * @date: 2023/5/8
 */
@Api(tags = "教师管理")
@RestController
@RequestMapping(value = "/teacher")
public class TeacherController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherController.class);

    @Autowired
    private TeacherService teacherService;

    @ApiOperation(value = "根据id查询教师")
    @GetMapping(value = "/selectTeacherById/{id}")
    public Result<Teacher> selectTeacherById(@ApiParam(name = "id", value = "教师id", required = true)
                                             @PathVariable("id") Long id) {
        Teacher teacher = teacherService.selectTeacherById(id);
        return Result.build(teacher, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "查询所有教师")
    @GetMapping(value = "/selectAll")
    public Result<List<Teacher>> selectAll() {
        List<Teacher> teachers = teacherService.selectAll();
        return Result.build(teachers, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "条件查询教师，带分页")
    @PostMapping(value = "/selectTeachersByConditionPage/{page}/{limit}")
    public Result<Map<String, Object>> selectTeachersByConditionPage(@ApiParam(name = "page", value = "页码", required = true)
                                                                     @PathVariable("page") Long page,
                                                                     @ApiParam(name = "limit", value = "每页记录数", required = true)
                                                                     @PathVariable("limit") Long limit,
                                                                     @ApiParam(name = "teacherQueryVo", value = "查询条件vo类")
                                                                     @RequestBody(required = false) TeacherQueryVo teacherQueryVo) {
        Map<String, Object> map = teacherService.selectTeachersByConditionPage(page, limit, teacherQueryVo);
        return Result.ok(map);
    }

    @ApiOperation(value = "新增教师")
    @PostMapping(value = "/insertTeacher")
    public Result insertTeacher(@ApiParam(name = "teacherInsertVo", value = "教师新增实体", required = true)
                                @RequestBody TeacherInsertVo teacherInsertVo) {
        return teacherService.insertTeacher(teacherInsertVo) ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "根据id修改教师")
    @PutMapping(value = "/modifyTeacher")
    public Result modifyTeacher(@ApiParam(name = "teacherUpdateVo", value = "教师修改实体", required = true)
                                @RequestBody TeacherUpdateVo teacherUpdateVo) {
        return teacherService.modifyTeacher(teacherUpdateVo) ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "根据id逻辑删除教师")
    @PatchMapping(value = "/removeTeacherById/{id}")
    public Result removeTeacherById(@ApiParam(name = "id", value = "教师id", required = true)
                                    @PathVariable("id") Long id) {
        return teacherService.removeTeacherById(id) ? Result.ok() : Result.fail();
    }

    @ApiOperation(value = "批量删除教师")
    @PatchMapping(value = "/removeBatchByIds")
    public Result removeBatchByIds(@ApiParam(name = "teacherIds", value = "待删教师id集合", required = true)
                                   @RequestBody List<Long> teacherIds) {
        return teacherService.removeBatchByIds(teacherIds) ? Result.ok() : Result.fail();
    }

    //--------------------------- 以下接口只为回顾OpenFeign远程接口调用 ---------------------------

    @ApiOperation(value = "远程接口调用：根据id查询学生")
    @GetMapping(value = "/openFeign/student/findStudentById/{id}")
    public Result<Student> openFeignFindStudentById(@ApiParam(name = "id", value = "学生id", required = true)
                                                    @PathVariable("id") Long id) {
        Student student = teacherService.openFeignFindStudentById(id);
        return Result.build(student, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "远程接口调用：查询所有学生")
    @GetMapping(value = "/openFeign/student/findAllStudents")
    public Result<List<Student>> openFeignFindAllStudents() {
        List<Student> studentList = teacherService.openFeignFindAllStudents();
        return Result.build(studentList, ResultCodeEnum.SUCCESS);
    }

    @ApiOperation(value = "远程接口调用：条件查询学生，带分页")
    @PostMapping(value = "/openFeign/student/findStudentsByConditionPage/{page}/{limit}")
    public Result<Map<String, Object>> openFeignFindStudentsByConditionPage(@ApiParam(name = "page", value = "页码", required = true)
                                                                            @PathVariable("page") Long page,
                                                                            @ApiParam(name = "limit", value = "每页记录数", required = true)
                                                                            @PathVariable("limit") Long limit,
                                                                            @ApiParam(name = "studentQueryVo", value = "查询条件vo类")
                                                                            @RequestBody(required = false) StudentQueryVo studentQueryVo) {
        Map<String, Object> map = teacherService.openFeignFindStudentsByConditionPage(page, limit, studentQueryVo);
        return Result.ok(map);
    }
}
