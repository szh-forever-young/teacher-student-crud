package com.szh.edu.teacher.service;

import com.szh.model.Student;
import com.szh.model.Teacher;
import com.szh.vo.StudentQueryVo;
import com.szh.vo.TeacherInsertVo;
import com.szh.vo.TeacherQueryVo;
import com.szh.vo.TeacherUpdateVo;

import java.util.List;
import java.util.Map;

/**
 * @author: SongZiHao
 * @date: 2023/5/8
 */
public interface TeacherService {
    /**
     * 根据id查询教师
     * @param id 教师id
     * @return Teacher
     */
    Teacher selectTeacherById(Long id);

    /**
     * 查询所有教师
     * @return List<Teacher>
     */
    List<Teacher> selectAll();

    /**
     * 条件查询教师，带分页
     * @param page 页码
     * @param limit 每页记录数
     * @param teacherQueryVo 查询条件
     * @return Map<String, Object>
     */
    Map<String, Object> selectTeachersByConditionPage(Long page, Long limit, TeacherQueryVo teacherQueryVo);

    /**
     * 新增教师
     * @param teacherInsertVo 教师新增vo类
     * @return 是否新增成功
     */
    boolean insertTeacher(TeacherInsertVo teacherInsertVo);

    /**
     * 根据id修改教师
     * @param teacherUpdateVo 教师修改vo类
     * @return 是否修改成功
     */
    boolean modifyTeacher(TeacherUpdateVo teacherUpdateVo);

    /**
     * 根据id逻辑删除教师
     * @param id 教师id
     * @return 是否删除成功
     */
    boolean removeTeacherById(Long id);

    /**
     * 批量删除教师
     * @param teacherIds 待删教师id集合
     * @return 是否批量删除成功
     */
    boolean removeBatchByIds(List<Long> teacherIds);

    /**
     * 远程接口调用：根据id查询学生
     * @param id 学生id
     * @return Student
     */
    Student openFeignFindStudentById(Long id);

    /**
     * 远程接口调用：查询所有学生
     * @return List<Student> 学生列表
     */
    List<Student> openFeignFindAllStudents();

    /**
     * 远程接口调用：条件查询学生，带分页
     * @param page 页码
     * @param limit 每页记录数
     * @param studentQueryVo 查询条件
     * @return Map<String, Object>
     */
    Map<String, Object> openFeignFindStudentsByConditionPage(Long page, Long limit, StudentQueryVo studentQueryVo);
}
