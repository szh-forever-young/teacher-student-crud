package com.szh.edu.teacher.mapper;

import com.szh.model.Teacher;
import com.szh.vo.TeacherQueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2023/5/8
 */
public interface TeacherMapper {
    /**
     * 根据id查询教师
     * @param id 教师id
     * @return Teacher
     */
    Teacher selectTeacherById(@Param("id") Long id);

    /**
     * 查询所有教师
     * @return List<Teacher> 教师列表
     */
    List<Teacher> selectAll();

    /**
     * 条件查询教师，带分页
     * @param teacherQueryVo 查询条件
     * @return List<Teacher> 教师列表
     */
    List<Teacher> selectTeachersByConditionPage(TeacherQueryVo teacherQueryVo);

    /**
     * 新增教师
     * @param teacher 教师实体类
     * @return 新增之后的受影响行数，0失败，1成功
     */
    int insertTeacher(Teacher teacher);

    /**
     * 根据id修改教师
     * @param teacher 教师修改vo类
     * @return 修改之后的受影响行数，0失败，1成功
     */
    int modifyTeacher(Teacher teacher);

    /**
     * 根据id逻辑删除教师
     * @param id 教师id
     * @return 删除之后的受影响行数，0失败，1成功
     */
    int removeTeacherById(@Param("id") Long id);

    /**
     * 批量删除教师
     * @param teacherIds 待删教师id集合
     * @return 批量删除之后的受影响行数，等于teacherIds集合长度则表示成功，其余情况均表示失败
     */
    int removeBatchByIds(@Param("teacherIds") List<Long> teacherIds);
}
