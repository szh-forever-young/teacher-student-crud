package com.szh.edu.teacher.client;

import com.szh.common.result.Result;
import com.szh.model.Student;
import com.szh.vo.StudentQueryVo;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 远程调用 service-edu-student 模块的接口
 *
 * @author: SongZiHao
 * @date: 2023/7/3
 */
@FeignClient("service-edu-student")
public interface StudentFeignClient {

    @GetMapping(value = "/student/findStudentById/{id}")
    Result<Student> findStudentById(@PathVariable("id") Long id);

    @GetMapping(value = "/student/findAllStudents")
    Result<List<Student>> findAllStudents();

    @PostMapping(value = "/student/findStudentsByConditionPage/{page}/{limit}")
    Result<Map<String, Object>> findStudentsByConditionPage(@PathVariable("page") Long page,
                                                            @PathVariable("limit") Long limit,
                                                            @RequestBody(required = false) StudentQueryVo studentQueryVo);
}
