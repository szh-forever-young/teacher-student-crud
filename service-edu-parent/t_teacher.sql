/*
 Navicat Premium Data Transfer

 Source Server         : MyProject
 Source Server Type    : MySQL
 Source Server Version : 50605
 Source Host           : localhost:3306
 Source Schema         : education

 Target Server Type    : MySQL
 Target Server Version : 50605
 File Encoding         : 65001

 Date: 03/07/2023 14:11:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_teacher
-- ----------------------------
DROP TABLE IF EXISTS `t_teacher`;
CREATE TABLE `t_teacher`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '教师id，主键自增',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '教师姓名',
  `age` int(11) NOT NULL COMMENT '教师年龄',
  `sex` tinyint(1) NOT NULL COMMENT '教师性别，1：男，2：女',
  `intro` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '教师简介',
  `level` tinyint(1) NOT NULL COMMENT '教师职称，1：助教，2：讲师，3：副教授，4：教授',
  `course` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '教师所讲课程',
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '教师住址',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除，0：未删除，1：已删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_teacher
-- ----------------------------
INSERT INTO `t_teacher` VALUES (1, '张老师', 35, 1, '念PPT的老师', 2, '计算机组成原理，模拟电子技术', '河南省郑州市金水区', 0, '2023-05-05 15:33:31', '2023-05-08 15:42:01');
INSERT INTO `t_teacher` VALUES (2, '李老师', 48, 1, '抄代码的老师', 1, '数据结构，C语言，计算机导论', '河南省郑州市中原区', 0, '2023-05-05 15:34:16', '2023-05-08 15:42:02');
INSERT INTO `t_teacher` VALUES (3, '王老师', 31, 2, '讲笑话的老师', 2, '数据库，数据结构，Python', '河南省洛阳市涧西区', 0, '2023-05-05 15:35:07', '2023-05-08 15:42:02');
INSERT INTO `t_teacher` VALUES (4, '秦老师', 40, 2, '爱提问的老师', 3, '操作系统，Java，大数据，人工智能', '河南省洛阳市西工区', 0, '2023-05-05 15:36:11', '2023-05-08 15:42:03');
INSERT INTO `t_teacher` VALUES (5, '刘老师', 55, 1, '很牛逼的老师', 4, '编译原理，计算机网络，C语言，数据结构，操作系统', '河南省郑州市二七区', 0, '2023-05-05 15:36:54', '2023-05-08 15:51:07');
INSERT INTO `t_teacher` VALUES (6, '杜老师', 55, 2, '数学界的大佬教师', 4, '高等数学，线性代数，概率论', '河南省洛阳市老城区', 0, '2023-05-08 13:24:09', '2023-05-08 15:51:08');
INSERT INTO `t_teacher` VALUES (7, '宋老师', 27, 2, '第一天教课，但多姿多彩的老师', 1, '音乐，美术，体育，英语听说读写，语文', '河南省洛阳市涧西区', 0, '2023-05-08 15:47:28', '2023-05-08 15:51:09');
INSERT INTO `t_teacher` VALUES (8, '郭老师', 25, 1, '在社保科技上班的教师', 4, 'Java，计算机网络，数据库', '江苏省苏州市吴中区', 0, '2023-05-08 15:52:31', '2023-05-08 15:52:30');
INSERT INTO `t_teacher` VALUES (9, '崔老师', 39, 1, 'introintrointrointro', 2, 'Java，C，C++，MySQL，Python', '北京市海淀区', 0, '2023-05-08 15:53:20', '2023-05-08 15:53:19');
INSERT INTO `t_teacher` VALUES (10, '乔老师', 58, 1, '很老的老师', 4, '什么都能讲，但你不一定能听懂', '广东省深圳市南山区', 0, '2023-05-08 15:54:11', '2023-05-08 15:54:10');
INSERT INTO `t_teacher` VALUES (11, '屈老师', 25, 2, 'testtesttest', 1, '英语视听说，英语读写', '浙江省杭州市余杭区', 0, '2023-05-08 15:55:01', '2023-05-08 15:55:01');
INSERT INTO `t_teacher` VALUES (12, '陈老师', 46, 1, 'teacherteacherteacher', 3, '物理，化学，生物', '陕西省西安市雁塔区', 0, '2023-05-08 15:55:47', '2023-05-08 15:55:47');

SET FOREIGN_KEY_CHECKS = 1;
