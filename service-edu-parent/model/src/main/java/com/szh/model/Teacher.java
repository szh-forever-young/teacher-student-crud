package com.szh.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 教师实体类
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@ApiModel(description = "教师实体")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Teacher extends BaseEntity {

    @ApiModelProperty(value = "教师姓名")
    private String name;

    @ApiModelProperty(value = "教师年龄")
    private Integer age;

    @ApiModelProperty(value = "教师性别")
    private Integer sex;

    @ApiModelProperty(value = "教师简介")
    private String intro;

    @ApiModelProperty(value = "教师职称")
    private Integer level;

    @ApiModelProperty(value = "教师所讲课程")
    private String course;

    @ApiModelProperty(value = "教师住址")
    private String address;
}
