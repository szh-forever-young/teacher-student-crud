package com.szh.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * model实体类的超类，抽取所有实体类的公共属性
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@ApiModel(description = "公共实体")
@Data
public class BaseEntity {

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "逻辑删除字段")
    private Integer isDeleted;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
