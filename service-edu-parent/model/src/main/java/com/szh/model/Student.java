package com.szh.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 学生实体类
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@ApiModel(description = "学生实体")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Student extends BaseEntity {

    @ApiModelProperty(value = "学生姓名")
    private String name;

    @ApiModelProperty(value = "学生年龄")
    private Integer age;

    @ApiModelProperty(value = "学生性别")
    private Integer sex;

    @ApiModelProperty(value = "学生简介")
    private String intro;

    @ApiModelProperty(value = "学生爱好")
    private String hobby;

    @ApiModelProperty(value = "学生住址")
    private String address;
}


