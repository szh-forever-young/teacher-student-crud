package com.szh.vo;

import lombok.Data;

import java.util.Date;

/**
 * 封装学生查询条件的vo类
 *
 * @author: SongZiHao
 * @date: 2023/5/6
 */
@Data
public class StudentQueryVo {
    private String name;
    private Integer sex;
    private String address;
    private Date createTimeBegin;
    private Date createTimeEnd;
}
