package com.szh.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/5/7
 */
@Data
public class StudentUpdateVo {
    private Long id;
    private String name;
    private Integer age;
    private Integer sex;
    private String intro;
    private String hobby;
    private String address;
}
