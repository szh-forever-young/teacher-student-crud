package com.szh.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author: SongZiHao
 * @date: 2023/5/8
 */
@Data
public class TeacherQueryVo {
    private String name;
    private Integer level;
    private String course;
    private String address;
    private Date createTimeBegin;
    private Date createTimeEnd;
}
