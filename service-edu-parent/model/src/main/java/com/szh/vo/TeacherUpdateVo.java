package com.szh.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/5/8
 */
@Data
public class TeacherUpdateVo {
    private Long id;
    private String name;
    private Integer age;
    private Integer sex;
    private String intro;
    private Integer level;
    private String course;
    private String address;
}
